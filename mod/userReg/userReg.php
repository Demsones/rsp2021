<?PHP 
///if (!defined('opentestsystem')) {    die();}


$backlink="?";

echo <<<DT
<h2 style="margin: 10px">Регистрация нового пользователя</h2>
DT;


$id=$_GET["id"];

//?mod=edit&id=5
function mask_phone($phone) {
	$array1 = array("(", ")", " ", "-", "+7");
	$array2 = array("", "", "", "", "8");
	return str_replace($array1, $array2, $phone);
}
function validate_phone($phone){
 $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);/*Удаляем все символы, кроме цифр и знаков плюса и минуса*/
 $phone_to_check = str_replace("-", "", $filtered_phone_number);

 if(iconv_strlen($phone,'UTF-8')!=preg_match_all("/[^A-Za-zА-Яа-яёЁ]/", $phone, $out)){/*проверим, совпадают ли по количеству символом строка чистая и очищенная от букв*/
  return false;
 }
 if(strlen($phone_to_check) < 10 || strlen($phone_to_check) > 20) {/*если НЕ меньше 10 символов и не больше 20*/
  return false;
 } else {
  return true;
 }
}

function validate_email($email){
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    return true;
} else {
    return false;
 }
}


$fl_name="fam";
$fl_val="Фамилия";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
    if (strlen($dt["$fl_name"])>100){
        $errorstring.="Длинна поля $fl_val превышает 100 символов <br>";
    }
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="im";
$fl_val="Имя";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
    if (strlen($dt["$fl_name"])>100 || strlen($dt["$fl_name"])<2){
        $errorstring.="Длинна поля $fl_val превышает 100 символов или меньше 2-х символов<br>";
    }
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="otch";
$fl_val="Отчество";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
    if (strlen($dt["$fl_name"])>100){
        $errorstring.="Длинна поля $fl_val превышает 100 символов <br>";
    }
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}


$fl_name="mail";
$fl_val="Email";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
if (strlen($dt["$fl_name"])<5){
	$errorstring.="Длинна поля $fl_val меньше 5 <br>";
}
elseif (validate_email($dt["$fl_name"])) {
	}
	else  $errorstring.="$fl_val введенн не верно<br>";
}else{
    $errorstring.="Поле $fl_val пустое<br>";
}


$fl_name="ph";
$fl_val="Телефон";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
		if (!validate_phone($dt["$fl_name"])){
		 $errorstring.="Поле $fl_val введенно не верно<br>";
		 }
}else $errorstring.="Поле $fl_val пустое<br>";


$fl_name="pw";
$fl_val="Пароль";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
    if (strlen($dt["$fl_name"])< 5){
        $errorstring.="Длинна поля $fl_val меньше 5 символов <br>";
    }
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}


$fl_name="pww";
$fl_val="Проверочный пароль";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
    if (strlen($dt["$fl_name"])<5){
        $errorstring.="Длинна поля $fl_val меньше 5 символов <br>";
    }
	elseif($dt["$fl_name"]!=$dt["pw"]){
		$errorstring.="Пароли не совпадают<br>";
	}
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="drod";
$fl_val="Дата рождения";
if (isset($_POST["$fl_name"])){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{ $errorstring.="Поле $fl_val пустое<br>";}


$fl_name="rcode";
$fl_val="Проверочный код";

if (!isset($_SESSION["rcode"])) {
	$_SESSION["rcode"]=rand(1,99);
	$_SESSION["rcode1"]=rand(1,99);
	$_SESSION["rcode2"]=rand(1,99);
	$_SESSION["rcode3"]=rand(1,99);
}

if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
if ($dt["$fl_name"]==$_SESSION["rcode"]){
}
	else $errorstring.="$fl_val введен не верно<br>";
}else $errorstring.="Поле $fl_val пустое<br>";







$sql="SELECT * FROM `user` WHERE `im` LIKE '{$dt["im"]}' AND `email` LIKE '{$dt["mail"]}'";
//echo "$sql" ;
$result = $mysqli->query($sql);
if ($result->num_rows !=0) {
$errorstring.="Пользователь с таким email и именем уже существует<br>";
}


// блок выбора этапа степ 

$step=1;
 
if (isset($_POST["s2"]) && $errorstring==""){$step=2;}

if (isset($_POST["s3"]) && $errorstring==""){$step=3;}

if (!isset($_POST["im"]) && $step==1){$errorstring="";
$_SESSION["rcode"]=rand(1,99);
$_SESSION["rcode1"]=rand(1,99);
$_SESSION["rcode2"]=rand(1,99);
$_SESSION["rcode3"]=rand(1,99);
}
if (isset($_SESSION["uid"])){$step=4;}
 














// блок отображения
if ($step==1){
if ($errorstring!=""){$errorstring="<font color=red>Ошибка!<br>".
    $errorstring."</font>";}
	$dt["im"] = htmlspecialchars($dt["im"]);
	$dt["fam"] = htmlspecialchars($dt["fam"]);
	$dt["otch"] = htmlspecialchars($dt["otch"]);
	$dt["mail"] = htmlspecialchars($dt["mail"]);
	$dt["ph"] = htmlspecialchars($dt["ph"]);
	$dt["pw"] = htmlspecialchars($dt["pw"]);
	$dt["pww"] = htmlspecialchars($dt["pww"]);
	$dt["drod"] = htmlspecialchars($dt["drod"]);
	$dt["rcode"] = htmlspecialchars($dt["rcode"]);
	

	
	
echo <<<DaT

<form method="post">
$errorstring
<div class="row reg-table">
	<div class="col-12 col-md-10 col-xl-7">
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Имя
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="im" size="100" value="{$dt["im"]}" />
			</div>
		</div>

		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Фамилия
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="fam" size="100" value="{$dt["fam"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Отчество
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="otch" size="100" value="{$dt["otch"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Email адрес
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="email" name="mail" size="106" value="{$dt["mail"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Телефон
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="tel" name="ph" size="100" value="{$dt["ph"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Дата рождения
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="date" name="drod" value="{$dt["drod"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Пароль
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="password" name="pw" value="{$dt["pw"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				Подтвердите пароль
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="password" name="pww" value="{$dt["pww"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px; margin: 10px">
				({$_SESSION["rcode1"]} + {$_SESSION["rcode"]} - {$_SESSION["rcode2"]} = {$_SESSION["rcode3"]}) введите 2 цифру 
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="rcode" size="100" value="{$dt["rcode"]}" />
			</div>
		</div>
		<br>
		<div class="row align-items-center reg-row">
			<div class="col-12">
				<input style="border-radius: 10px; font-size: 20px; padding: 10px; margin: 10px; background: #FFFFFF; text-align: center;" class='button' type="submit" name="s2" value="Продолжить" />
				<a class='link'href="$backlink" style="border: 2px solid #000000; display: inline-block; text-decoration: none; color: #000000; background: #FFFFFF; border-radius: 10px; font-size: 20px; padding: 10px; margin: 10px; text-align: center;">Вернуться</a>
			</div>
		</div>
	</div>
</div>
</form>
DaT;
}
if ($step==2){
	$dt["im"] = htmlspecialchars($dt["im"]);
	$dt["fam"] = htmlspecialchars($dt["fam"]);
	$dt["otch"] = htmlspecialchars($dt["otch"]);
	$dt["mail"] = htmlspecialchars($dt["mail"]);
	$dt["ph"] = htmlspecialchars($dt["ph"]);
	$dt["ph"] = mask_phone($dt["ph"]);
	$dt["drod"] = htmlspecialchars($dt["drod"]);

echo <<<DT
<form method="post" >
$errorstring

<div class="row reg-table">
	<div class="col-12">
		Подтвердите введенные данные
	</div>
	<div class="col-12 col-md-10 col-xl-7">
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Имя
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["im"]}</span>
				<input type="hidden" name="im"  value="{$dt["im"]}" />
			</div>
		</div>
		

		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Фамилия
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["fam"]}</span>
				<input type="hidden" name="fam"  value="{$dt["fam"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Отчество
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["otch"]}</span>
				<input type="hidden" name="otch"  value="{$dt["otch"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Email адрес
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["mail"]}</span>
				<input type="hidden" name="mail"  value="{$dt["mail"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Телефон
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["ph"]}</span>
				<input type="hidden" name="ph"  value="{$dt["ph"]}" />
			</div>
		</div>
		
		<div class="row align-items-center reg-row">
			<div class="col-12 col-md-4" style="font-size: 20px">
				Дата рождения
			</div>
			<div class="col-12 col-md-8">
				<span style="font-size: 20px" class='medium'>{$dt["drod"]}</span>
				<input type="hidden" name="drod"  value="{$dt["drod"]}" />
				<input type="hidden" name="pw"  value="{$dt["pw"]}" />
				<input type="hidden" name="pww"  value="{$dt["pww"]}" />
				<input type="hidden" name="rcode"  value="{$dt["rcode"]}" />
			</div>
		</div>

		<div class="row align-items-center reg-row">
			<div class="col-12">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; margin-left: 80px; font-size: 20px" class='button' type="submit" name="s3" value="Продолжить" />
			<input style="background-color: #61B4CF; width: 400px; height: 35px; margin-left: 80px; font-size: 20px" class='button' type="submit" name="s0" value="Вернуться" />
			</div>
		</div>
	</div>
</div>
</form>
DT;
}
if ($step==3){

$sql="SELECT MAX(`id`) FROM users;";
$result = $mysqli->query($sql);
	if ($result->num_rows ==0) {
	     $id=1;
	}else{
		$razdel_table = $result->fetch_assoc();
	//print_r($razdel_table);
		$id=$razdel_table["MAX(`id`)"]+1;
	}


$dt["pw"]=md5($dt["pw"]);

$sql="SELECT * FROM `users` WHERE `fam` LIKE '{$dt["fam"]}' AND `im` LIKE '{$dt["im"]}' AND `otch` LIKE '{$dt["otch"]}' AND `email` LIKE '{$dt["mail"]}' AND `phone` LIKE '{$dt["ph"]}' AND `pw` LIKE '{$dt["pw"]}' AND `bdate` LIKE '{$dt["drod"]}'";
$result = $mysqli->query($sql);
if ($result->num_rows ==0) {

			//Таблица. testsection: *id, name, userid, parent_id, description.
			$sql="INSERT INTO `users` (`id`, `fam`, `im`, `otch`, `email`, `phone`, `pw`, `bdate`) VALUES ('$id', '{$dt["fam"]}', '{$dt["im"]}', '{$dt["otch"]}', '{$dt["mail"]}', '{$dt["ph"]}', '{$dt["pw"]}', '{$dt["drod"]}');";
			$result = $mysqli->query($sql);
			if(!$result){
			echo "bad sql ***$sql**";

			}else{

			echo <<<DT
			<p style="font-size: 24px; font-family: Bookman Old Style">Новый пользователь успешно зарегистрирован.</p> 
			<button style="background: -moz-linear-gradient(#D0ECF4, #5BC9E1, #D0ECF4); background: -webkit-gradient(linear, 0 0, 0  100%, from(#D0ECF4), to(#D0ECF4), color-stop(0.5, #5BC9E1)); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00BBD6', endColorstr='#EBFFFF'); padding: 3px 7px; color: #333; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; border: 1px solid #666;"> 
			<a style="font-size: 24px; font-family: Bookman Old Style; text-decoration: none; color: #000000" href="$backlink">Вернуться
			</a>
			</button>
DT;
			}
}else{
		echo <<<DT
			пользователь уже зарегистрирован, войдите в систему.
DT;

}
}



if ($step==4){
 	echo <<<DT
			<p style="font-size: 24px; font-family: Bookman Old Style">пользователь уже авторизован. сначала необходимо выйти из системы</p> 
			<button style="background: -moz-linear-gradient(#D0ECF4, #5BC9E1, #D0ECF4); background: -webkit-gradient(linear, 0 0, 0  100%, from(#D0ECF4), to(#D0ECF4), color-stop(0.5, #5BC9E1)); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00BBD6', endColorstr='#EBFFFF'); padding: 3px 7px; color: #333; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; border: 1px solid #666;"> 
			<a style="font-size: 24px; font-family: Bookman Old Style; text-decoration: none; color: #000000" href="?mod=userLogout">Вернуться
			</a>
			</button>
DT;
}















?>