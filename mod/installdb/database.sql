-- phpMyAdmin SQL Dump
-- version 5.2.0-dev
-- https://www.phpmyadmin.net/
--
-- Хост: 192.168.30.23
-- Время создания: Апр 08 2021 г., 20:29
-- Версия сервера: 8.0.18
-- Версия PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: ` database`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `idzak` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fam` text NOT NULL,
  `im` text NOT NULL,
  `otch` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `pw` text NOT NULL,
  `bdate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `zacazcik`
--

CREATE TABLE `zacazcik` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `cont` text NOT NULL,
  `req` text NOT NULL,
  `date` int(11) NOT NULL,
  `idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `zak`
--

CREATE TABLE `zak` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `state` text NOT NULL,
  `idzakazcik` int(11) NOT NULL,
  `datestart` int(11) NOT NULL,
  `datestop` int(11) NOT NULL,
  `place` text NOT NULL,
  `info` text NOT NULL,
  `price` int(11) NOT NULL,
  `hprior` text NOT NULL,
  `hinfo` text NOT NULL,
  `spisokzak` text NOT NULL,
  `idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `zaklink`
--

CREATE TABLE `zaklink` (
  `id` int(11) NOT NULL,
  `idzak` int(11) NOT NULL,
  `link` text NOT NULL,
  `usl` text NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
