-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 10 2021 г., 18:38
-- Версия сервера: 8.0.19
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `rsp2021`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chat`
--

CREATE TABLE `chat` (
  `id` int NOT NULL,
  `idzak` int NOT NULL,
  `iduser` int NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `fam` text NOT NULL,
  `im` text NOT NULL,
  `otch` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `pw` text NOT NULL,
  `bdate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `fam`, `im`, `otch`, `email`, `phone`, `pw`, `bdate`) VALUES
(1, 'Гудима', 'Илья', 'Алексеевич', 'lilgud@mail.ru', '89108830416', '4297f44b13955235245b2497399d7a93', '');

-- --------------------------------------------------------

--
-- Структура таблицы `zacazcik`
--

CREATE TABLE `zacazcik` (
  `id` int NOT NULL,
  `name` text NOT NULL,
  `cont` text NOT NULL,
  `req` text NOT NULL,
  `date` int NOT NULL,
  `idusers` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `zak`
--

CREATE TABLE `zak` (
  `id` int NOT NULL,
  `name` text NOT NULL,
  `state` text NOT NULL,
  `idzakazcik` int DEFAULT NULL,
  `datestart` date DEFAULT NULL,
  `datestop` date DEFAULT NULL,
  `place` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `info` text NOT NULL,
  `price` int DEFAULT '0',
  `hprior` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `hinfo` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `spisokzak` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `idusers` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zak`
--

INSERT INTO `zak` (`id`, `name`, `state`, `idzakazcik`, `datestart`, `datestop`, `place`, `info`, `price`, `hprior`, `hinfo`, `spisokzak`, `idusers`) VALUES
(1, 'Заказ1', '', NULL, '2021-06-10', '2021-06-04', '', '', 0, NULL, NULL, NULL, 1),
(2, 'Заказ2', '', NULL, '2021-06-10', '2021-06-10', '', '', 0, NULL, NULL, NULL, 1),
(3, 'Заказ3', '', NULL, '2021-06-10', '2021-06-10', '', '', 0, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `zaklink`
--

CREATE TABLE `zaklink` (
  `id` int NOT NULL,
  `idzak` int NOT NULL,
  `link` text NOT NULL,
  `usl` text NOT NULL,
  `date` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
