<?PHP 
///if (!defined('opentestsystem')) {    die();}


$backlink="?";

echo <<<DT
<h2>Регистрация нового заказа</h2>
DT;

////

$fl_name="namezak";
$fl_val="Наименование заказа";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]="";
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="datereg";
$fl_val="Дата регистрации заказа";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]=date("Y-m-d");
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="dateorder";
$fl_val="Дата поступления заказа";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]=date("Y-m-d");
    $errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="place";
$fl_val="Место назначение";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]="";
    //$errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="info";
$fl_val="Примечание к заказу";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]="";
    //$errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="price";
$fl_val="Стоимость";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]="";
    //$errorstring.="Поле $fl_val пустое<br>";
}

$fl_name="st";
$fl_val="Статус";
if (isset($_POST["$fl_name"]) && $_POST["$fl_name"]!=""){
    $dt["$fl_name"]=trim($_POST["$fl_name"]);
	$dt["$fl_name"]= $mysqli->real_escape_string($dt["$fl_name"]);
}else{
    $dt["$fl_name"]="";
    //$errorstring.="Поле $fl_val пустое<br>";
}


$sql="SELECT * FROM `zak` WHERE `name` LIKE '{$dt["namezak"]}' ";
//echo "$sql" ;
$result = $mysqli->query($sql);
if ($result->num_rows !=0) {
$errorstring.="Заказ с таким именем  уже существует<br>";
}


//

// блок выбора этапа степ 

$step=1;
 
if (isset($_POST["s2"]) && $errorstring==""){$step=2;}

if (isset($_POST["s3"]) && $errorstring==""){$step=3;}

if (!isset($_POST["$fl_name"]) && $step==1){$errorstring="";
}

if (is_user()==false){$step=4;}



// блок отображения
if ($step==1){
	if ($errorstring!=""){$errorstring="<font color=red>Ошибка!<br>".
		$errorstring."</font>";}
		$dt["namezak"] = htmlspecialchars($dt["namezak"]);
		$dt["datereg"] = htmlspecialchars($dt["datereg"]);
		$dt["dateorder"] = htmlspecialchars($dt["dateorder"]);
		$dt["place"] = htmlspecialchars($dt["place"]);
		$dt["info"] = htmlspecialchars($dt["info"]);
		$dt["price"] = htmlspecialchars($dt["price"]);
		$dt["st"] = htmlspecialchars($dt["st"]);
	
		
		
	
echo <<<DaT

<form method="post">
$errorstring
<form>
<div class="row zak-table">
	<div class="col-12 col-md-10 col-xl-7">
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Наименование заказа
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="namezak" size="100" value="{$dt["namezak"]}" />
			</div>
		</div>

		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Дата регистрации заказа
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="date" name="datereg" size="100" value="{$dt["datereg"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Дата поступления заказа
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="date" name="dateorder" size="100" value="{$dt["dateorder"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Место назначение
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="place" size="100" value="{$dt["place"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Примечание к заказу 
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="info" size="100" value="{$dt["info"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Стоимость
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="price" value="{$dt["price"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Статус
			</div>
			<div class="col-12 col-md-8">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; font-size: 20px" type="text" name="st" value="{$dt["st"]}" />
			</div>
		</div>
		
		

		<div class="row align-items-center zak-row">
			<div class="col-12">
				<a class='opacity-btn m-left'href="$backlink"><img src="../../Image/Вернуться 1.png" width="30" height="30""></a>
				<input style="background-color: #61B4CF; width: 400px; height: 35px; margin-left: 20px; font-size: 20px" class='inline-block' type="submit" name="s2" value="Продолжить" />
			</div>
		</div>
	</div>
</div>
</form>

DaT;
}
if ($step==2){
		$dt["namezak"] = htmlspecialchars($dt["namezak"]);
		$dt["datereg"] = htmlspecialchars($dt["datereg"]);
		$dt["dateorder"] = htmlspecialchars($dt["dateorder"]);
		$dt["place"] = htmlspecialchars($dt["place"]);
		$dt["info"] = htmlspecialchars($dt["info"]);
		$dt["price"] = htmlspecialchars($dt["price"]);
		$dt["st"] = htmlspecialchars($dt["st"]);
		
echo <<<DT
<form method="post" >
$errorstring

<form>
<div class="row zak-table">
	<div class="col-12 col-md-10 col-xl-7">
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Наименование заказа
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["namezak"]}</span>
				<input type="hidden" name="namezak" size="100" value="{$dt["namezak"]}" />
			</div>
		</div>

		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Дата регистрации заказа
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["datereg"]}</span>
				<input type="hidden" name="datereg" size="100" value="{$dt["datereg"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Дата поступления заказа
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["dateorder"]}</span>
				<input type="hidden" name="dateorder" size="100" value="{$dt["dateorder"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Место назначение
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["place"]}</span>
				<input type="hidden" name="place" size="100" value="{$dt["place"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Примечание к заказу 
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["info"]}</span>
				<input type="hidden" name="info" size="100" value="{$dt["info"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Стоимость
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["price"]}</span>
				<input type="hidden" name="price" value="{$dt["price"]}" />
			</div>
		</div>
		
		<div class="row align-items-center zak-row">
			<div class="col-12 col-md-4">
				Статус
			</div>
			<div class="col-12 col-md-8">
				<span class='medium'>{$dt["st"]}</span>
				<input type="hidden" name="st" value="{$dt["st"]}" />
			</div>
		</div>
		
		

		<div class="row align-items-center zak-row">
			<div class="col-12">
				<input style="background-color: #61B4CF; width: 400px; height: 35px; margin-left: 20px; font-size: 20px" class='inline-block' type="submit" name="s3" value="Продолжить" />
				<input style="background-color: #61B4CF; width: 400px; height: 35px; margin-left: 20px; font-size: 20px" class='button' type="submit" name="s0" value="Вернуться" />
			</div>
		</div>
	</div>
</div>
</form>
DT;
}
if ($step==3){

$sql="SELECT MAX(`id`) FROM zak;";
$result = $mysqli->query($sql);
if(!$result){
			echo "bad sql ***$sql**";}

	if ($result->num_rows ==0) {
	     $id=1;
	}else{
		$razdel_table = $result->fetch_assoc();
	//print_r($razdel_table);
		$id=$razdel_table["MAX(`id`)"]+1;
	}



if($dt["price"]==''){
	$dt["price"]="0";
}
			$uid=get_userid();
			$sql="INSERT INTO `zak` (`id`, `name`, `state`, `idzakazcik`, `datestart`, `datestop`, `place`, `info`, `price`, `hprior`, `hinfo`, `spisokzak`, `idusers`) VALUES ('$id', '{$dt["namezak"]}', '{$dt["st"]}', null, '{$dt["datereg"]}', '{$dt["dateorder"]}', '{$dt["place"]}', '{$dt["info"]}', '{$dt["price"]}', null, null, null, '$uid');";
			$result = $mysqli->query($sql);
			if(!$result){
			echo "bad sql ***$sql**";

			}else{

			echo <<<DT
			<p style="font-size: 24px; font-family: Bookman Old Style">Новый заказ успешно добавлен.</p> 
			<button style="background: -moz-linear-gradient(#D0ECF4, #5BC9E1, #D0ECF4); background: -webkit-gradient(linear, 0 0, 0  100%, from(#D0ECF4), to(#D0ECF4), color-stop(0.5, #5BC9E1)); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00BBD6', endColorstr='#EBFFFF'); padding: 3px 7px; color: #333; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; border: 1px solid #666;"> 
			<a style="font-size: 24px; font-family: Bookman Old Style; text-decoration: none; color: #000000" href="$backlink">Вернуться
			</a>
			</button>
DT;
			}



}


if ($step==4){
echo 'пользователь не авторизован';
}
?>